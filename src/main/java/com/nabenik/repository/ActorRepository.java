package com.nabenik.repository;

import com.nabenik.model.Actor;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@RequestScoped
@Default
public class ActorRepository {

    @Inject
    EntityManager em;


    public List<Actor> listAll() {
        String query = "SELECT m FROM Movie m";

        Query typedQuery = em.createQuery(query);

        return typedQuery.getResultList();
    }

    public Actor findById(long id) {
        return em.find(Actor.class,id);
    }

    public void create(Actor actor) {
        em.persist(actor);
    }

    public Actor update(Actor actor) {
        return em.merge(actor);
    }

    public void delete(Actor actor) {
        em.remove(actor);
    }

}
