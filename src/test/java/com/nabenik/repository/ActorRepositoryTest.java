package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static WebArchive createDeployment(){
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(MovieRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");

        System.out.println(war.toString(true));

        return war;
    }

    //listAll integration test. This must return 0 because there is not database to retrieve data
    @Test
    public void listAll() {
        List<Actor> movieList = actorRepository.listAll();

        System.out.println("Actor count " + movieList.size());

        assertEquals(0,movieList.size());
    }
    //Movie creation integration test
    @Test
    public void create() {
        Actor actor = new Actor("Samuel Jackson", "2014", "01/01/1960");
        actorRepository.create(actor);

        System.out.println("Actor Id " + actor.getActorId());

        assertNotNull(actor.getActorId());
    }
    //Movie update integration test
    @Test
    public void update() {
        Actor actor = new Actor("Samuel Jackson", "2014", "01/01/1960");
        actorRepository.create(actor);
        actor.setName("Samuel L. Jackson");
        actorRepository.update(actor);

        System.out.println("Actor name " + actor.getName());

        assertEquals(actor.getName(),"Samuel L. Jackson");
    }
    // Movie delete integration test
    @Test
    public void delete() {
        Actor actor = new Actor("Samuel Jackson", "2014", "01/01/1960");
        actorRepository.delete(actor);
        //must be null because there is no data
        assertNull(actor.getActorId());
    }
    //FindByID integrationTest
    @Test
    public void findById() {
        Actor actor = new Actor("Samuel Jackson", "2014", "01/01/1960");
        actorRepository.create(actor);

        Actor searchedActor = actorRepository.findById(actor.getActorId());

        assertNotNull(searchedActor.getActorId());
    }

}
