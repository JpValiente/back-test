package com.nabenik.controller;

import io.restassured.http.ContentType;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;


public class MovieControllerTest extends TestCase {

    //test for search
    public void testFindById() {
        when().get("/back-test/rest/movies/{id}",10).
            then().statusCode(204);
    }

    //Test for creation
    public void testCreate() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("title","Pulp fiction");
        requestParams.put("year","1994");
        requestParams.put("duration","2h 34m");
        given().
            accept("application/json").
            contentType("application/json").
            and().body(requestParams).
            when().post("/back-test/rest/movies").
            then().statusCode(201);
    }
    //test for Update
    public void testUpdate() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("title","Pulp fiction 2");
        requestParams.put("year","2021");
        requestParams.put("duration","2h 34m");
        given().
            accept("application/json").
            contentType("application/json").
            and().body(requestParams).
            when().put("/back-test/rest/movies/{id}",10).
            then().statusCode(201);
    }
    //test for delete
    //In this particular test, we expect a 500 status because the id doesn't exist. Is the error that we can find using postman.
    public void testDelete() {
        given()
            .header("Content-type", "application/json")
            .when().delete("/back-test/rest/movies/10").
            then().statusCode(500);
    }
}