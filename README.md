# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* JavaScript knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations

- JSON-P

> Is a Javascript technique that allows to request data by using the tag \<script>, and it permits cross-domain requests. Implementation:
- CDI

> CDI stands for Contexts and Dependency Injection. Is a design pattern that allows us to manage the lifecycle of components and inject other components, like services, into client objects in a safe and efficient way. Implementation:

- JPA
> JPA stands for Java Persistence API. It's Java's way to implement an ORM, that allows to interact with the database using Java objects. Implementation:
- JAX-RS
> JAX-RS is an API that gives us support in the developing of web services, using the design pattern Representational State Transfer (REST) Implementation:
3. Which of the following is/are not an application server?
> Tomcat, because it is a Servlet container Quarkus, because it's a microservice framework Helidon and KumuluzEE are kind of tricky, because they have embed an application server, but they are Microservice frameworks

* Helidon
* Quarkus
* WebSphere
* Tomcat
* KumuluzEE

4. In your opinion what's the main benefit of moving from Java 8 to Java 11
> Besides Java 11 is the next LTS version of Java after Java 8, the main benefit is the optimization of the usage of heap and Heap allocation on alternative memory devices. This reduces the chances of founding the OutOfMemory Exception.
5. In your opinion what's the main benefit of using TypeScript over JavaScript
> The benefit is helping in code organizing, allowing us to detect error or mistakes while coding and not at the runtime, like what vanilla JavaScript does
6. What's the main difference between OpenJDK and HotSpot
> They're basically the same, only that Hotspot offers some enterprise features that we cannot found in OpenJDK. And Hotspot it's paid.
7. If no database is configured? Will you be able to run this project? Why?
> Yes, it'll be able to run the project, however it'll throw a Socket Exception depending of the database we're using.

## Development tasks

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11 and  NodeJS 16
   
We have to update the repository in order to install NodeJS 16

![NodeJS1](screenshots/Nodejs 1.png)
![NodeJS2](screenshots/Nodejs 2.png)

OpenJDK. Since the most recent version of JDK is 17, we have to specify the version that we need, in this case Java 11.

![OpenJDK](screenshots/OpenJDK.png)
   
1. (easy) Build this project on a regular CLI over OpenJDK 11

![Maven1](screenshots/mvn 1.png)
![Maven2](screenshots/mvn 2.png)

2. (easy) Run this project using an IDE/Editor of your choice

![IDE](screenshots/IDE build.png)

3. (medium) This project has been created using Java EE APIs, please identify at least four APIs, bump it to Java EE 8 and Java 11, later run it over regular Payara Application Server 

I could identify: JUnit, Arquillian BOM, WeLogic and Java EE Itself.

![ParayaDeploy](screenshots/payara deploy.png)
![Run](screenshots/Deploy)

3. (medium) Execute the movie endpoint operations using a client -e.g. Postman-, if needed please also check/fix the code to be REST compliant

![GET](screenshots/api movies get id.png)
![POST](screenshots/api movies post.png)
![PUT](screenshots/put id.png)
![DELETE](screenshots/delete id.png)

4. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer

![Angular](screenshots/CRUD Angular.png)



5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

![Test](screenshots/First Test.png)
![MakingOfTests](screenshots/tests runs.png)

The implementation of the tests can be founded in the repository

6. (hard) Based on `MovieRepository` Integration Test, please create an integration test using [RestAssured](https://rest-assured.io/) and `MovieController`

![RestAssured](screenshots/Tests with rest assured.png)

7. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test

![ActorsAPI](screenshots/Actors' API Working.png)


This is a little test of the api, you can see the rest in the repo. The tests of actor repository are also there
8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Oracle Helidon](https://helidon.io/). Do it and don't port Integration Tests 

