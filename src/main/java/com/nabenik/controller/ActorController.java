package com.nabenik.controller;

import com.nabenik.model.Actor;
import com.nabenik.repository.ActorRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/actors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActorController {

    @Inject
    ActorRepository actorRepository;

    @GET
    public List<Actor> listAll() {
        return actorRepository.listAll();
    }

    @GET
    @Path("/{id}")
    public Actor findById(@PathParam("id") Long id) {
        return actorRepository.findById(id);
    }

    @POST
    public Response create(Actor actor) {
        actorRepository.create(actor);
        return Response.ok("Actor was saved").build();
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, Actor actor) {
        actorRepository.update(actor);
        return Response.ok("Actor was updated").build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        //this line causes a status code 500 because it can't find the movie beacuse there is not a DB
        Actor actor = actorRepository.findById(id);
        actorRepository.delete(actor);
        return Response.ok().build();
    }
}
